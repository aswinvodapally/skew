package com.stallion.skew.skewsample

import android.content.Context
import android.graphics.*
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View


class SkewLayout(context: Context?) : ConstraintLayout(context) {

    constructor(context: Context, attrs: AttributeSet) : this(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): this(context, attrs) {
        init()
    }

    private fun init() {

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

    }

    override fun drawChild(canvas: Canvas?, child: View?, drawingTime: Long): Boolean {
        return super.drawChild(canvas, child, drawingTime)
    }

}