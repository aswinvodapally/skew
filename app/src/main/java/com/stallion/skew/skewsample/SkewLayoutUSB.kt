package com.stallion.skew.skewsample


import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View


import java.lang.Math.PI
import android.graphics.*
import android.util.TypedValue
import android.graphics.Bitmap




/**
 * @author Pavan Kandi : 8th march,2017 : used to skew complete relative layout
 * @modified Bhargava Mandapati : 14th march,2017 : worked on comments & naming conventions
 */
class SkewLayoutUSB : ConstraintLayout {

    var degree: Float = 0.toFloat()
    var tan: Float = 0.toFloat()
    private var mContext: Context? = null

    private var angle: Float = 0.toFloat()
    private var pie = PI

    private var x1: Float = 0.toFloat()
    private var w: Float = 0.toFloat()
    private var y1: Float = 0.toFloat()
    private var h: Float = 0.toFloat()

    private var touchedX: Float = 0.toFloat()
    private var touchedY: Float = 0.toFloat()

    constructor(context: Context) : super(context) {
        mContext = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.SkewLayoutUSB, 0, 0)

        try {
            degree = a.getFloat(R.styleable.SkewLayoutUSB_slopedAngle, 0f)
            //if(degree==0) degree=15;

            setAngle(degree)
        } finally {
            a.recycle()
        }
    }

    fun setAngle(degree: Float) {
        tan = Math.tan(degree * pie / 180).toFloat()
        angle = degree
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val config = resources.configuration
        if (config.layoutDirection == View.LAYOUT_DIRECTION_RTL) {
            //in Right To Left layout
            canvas.skew(-tan, 0f)
        }
        else {
            canvas.skew(tan, 0f)
        }
        initVar()
    }

    fun initVar() {
        x1 = this.x
        y1 = this.y
        w = this.width.toFloat()
        h = this.height.toFloat()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        touchedX = ev.x
        touchedY = ev.y
        val finalX = touchedX - touchedY * tan
        if (finalX > 0 && finalX < w) {
            ev.setLocation(finalX, touchedY)
            return super.dispatchTouchEvent(ev)
        }
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        var needToStop = false
        val touchedX = ev.x
        val touchedY = ev.rawY
        val skewHeight = (touchedY / 8).toInt()
        val v = getChildAt(0).parent as View

        val skewtextleft = getChildAt(0).left + skewHeight
        val skewtextright = getChildAt(0).right + skewHeight

        needToStop = touchedX > skewtextleft + 30
        return if (needToStop) {
            false
        } else super.onInterceptTouchEvent(ev)
    }

    override fun drawChild(canvas: Canvas?, child: View?, drawingTime: Long): Boolean {
        val chView = loadBitmapFromView(child!!)
        //canvas?.drawBitmap(chView, 0f, 0f, null)
        return super.drawChild(canvas, chView, drawingTime)
    }

    fun loadBitmapFromView(v: View): View {
        //if (v.measuredHeight <= 0) {
            v.measure(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
            val b = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight, Bitmap.Config.ARGB_8888)
            val c = Canvas(b)
            //val matrix = Matrix()
            //matrix.setSkew(-tan, 0f)
            //c.drawBitmap(b, matrix, Paint())
            c.skew(-tan, 0f)
            v.layout(0, 0, v.measuredWidth, v.measuredHeight)
            v.draw(c)
            return v
        //}
        /*val b = Bitmap.createBitmap(v.layoutParams.width, v.layoutParams.height, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        c.skew(-tan,0f)
        v.layout(v.left, v.top, v.right, v.bottom)
        v.draw(c)
        return b*/
    }

    /*private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val pdMode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    private val path = Path()

    override fun dispatchDraw(canvas: Canvas?) {
        val saveCount = canvas?.saveLayer(0f, 0f, width.toFloat(), height.toFloat(), null, Canvas.ALL_SAVE_FLAG)
        super.dispatchDraw(canvas)

        paint.xfermode = pdMode
        path.reset()
        path.moveTo(0f, height.toFloat())
        path.lineTo(width.toFloat(), height.toFloat())
        path.lineTo(width.toFloat(), height - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50f, resources.displayMetrics))
        path.close()
        canvas?.drawPath(path, paint)

        canvas?.restoreToCount(saveCount!!)
        paint.xfermode = null
    }*/

}
